require('jsdom-global')()
const assert = require('assert')
const fs = require('fs')
global.crypto = require('crypto').webcrypto

const baercodeParsing = require('../src/baercode_parsing')
const baercodeVerifier = require('../src/baercode_verifier')

const scratchDir = process.env.SCRATCH_DIR

const multiKeyBundleBinary = fs.readFileSync(scratchDir + '/cert_bundle.cose')
const cosePublicKey = fs.readFileSync(scratchDir + '/kms-ecdsa-cose.hex', 'utf8')

const b64baercode = fs.readFileSync(scratchDir + '/baercode.b64', 'utf8')

const coseKeyBuffer = baercodeParsing.toArrayBuffer(
    new Uint8Array(cosePublicKey.match(/.{1,2}/g).map(b => parseInt(b, 16)))
)

describe('Key bundle integration testing', function () {
    describe('signature is properly checked', function () {
        it('validates signature with cose key', function (done) {
            const signedCbor = baercodeParsing.uintArrayToCbor(multiKeyBundleBinary)
            baercodeParsing.isSignatureValidCosePublicKey(signedCbor, coseKeyBuffer).then(validSignature => {
                assert.strictEqual(validSignature, true)
                done()
            }).catch(err => console.log('Error in test: ' + err))
        })
    })
    describe('parsing of bundle', function () {
        it('parses key bundle to map', function (done) {
            baercodeParsing.validateAndParseKeyBundleCoseKey(coseKeyBuffer, multiKeyBundleBinary).then(result => {
                const [valid, bundleMap] = result
                assert.strictEqual(valid, true)

                const [version, signedBaerCode] = baercodeParsing.base64ToVersionAndSignedCbor(b64baercode) // eslint-disable-line no-unused-vars
                const keyId = baercodeParsing.getBase64KeyId(signedBaerCode)
                const keyDefined = bundleMap[keyId] !== 'undefined'
                assert.strictEqual(keyDefined, true)

                done()
            }).catch(err => console.log('Error in test: ' + err))
        })

        it('verifies a baercode with key from bundle', function (done) {
            const signedBundle = baercodeParsing.uintArrayToCbor(multiKeyBundleBinary)
            const bundle = baercodeParsing.decodePayloadFromSignedCbor(signedBundle)
            const bundleMap = baercodeParsing.parseKeyBundleToMap(bundle)
            const [version, signedBaerCode] = baercodeParsing.base64ToVersionAndSignedCbor(b64baercode) // eslint-disable-line no-unused-vars
            const keyId = baercodeParsing.getBase64KeyId(signedBaerCode)

            // Specifically verify that we can encode to a localstorage friendly format
            const localFormat = baercodeParsing.toLocalStorageFormat(bundleMap[keyId])
            const keyData = baercodeParsing.fromLocalStorageFormat(localFormat)

            baercodeParsing.validateDecodeAndDecryptSignedCborKeyData(signedBaerCode, keyData).then(result => {
                const [valid, cborData] = result
                assert.strictEqual(valid, true)

                const [validBaercode, baercode] = baercodeVerifier.cborToBaerCode(cborData)
                assert.strictEqual(validBaercode, true)
                assert.strictEqual(baercode.first_name, 'Max')
                assert.strictEqual(baercode.last_name, 'Mustermann')
                assert.strictEqual(baercode.procedure_operator, 'Testing GmbH')
                done()
            }).catch(err => console.log('Error in test: ' + err))
        })
    })
})
