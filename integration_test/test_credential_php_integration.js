/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require('jsdom-global')()
const assert = require('assert')
const fs = require('fs')
global.crypto = require('crypto').webcrypto

const baercodeParsing = require('../src/baercode_parsing')
const baercodeVerifier = require('../src/baercode_verifier')

const scratchDir = process.env.SCRATCH_DIR
const b64baercode = fs.readFileSync(scratchDir + '/php_baercode.b64', 'utf8')

const b64baercodeMalicious = fs.readFileSync(scratchDir + '/php_baercode_malicious_key.b64', 'utf8')
const publicKeyPem = fs.readFileSync(scratchDir + '/php_baercode_pubkey.pem', 'utf8')
const aesKey = fs.readFileSync(scratchDir + '/php_baercode_cryptkey.hex', 'utf8')

const aesKeyBuffer = baercodeParsing.toArrayBuffer(
    new Uint8Array(aesKey.match(/.{1,2}/g).map(b => parseInt(b, 16)))
)

describe('php credential integration testing', function () {
    describe('signature is properly checked', function () {
        it('allows a baercode with valid signature', function (done) {
            const [version, signedCbor] = baercodeParsing.base64ToVersionAndSignedCbor(b64baercode)
            assert.strictEqual(version, 1)
            baercodeParsing.isSignatureValidPemPublicKey(signedCbor, publicKeyPem).then(validSignature => {
                assert.strictEqual(validSignature, true)
                done()
            }).catch(err => console.log('Error in test: ' + err))
        })

        it('rejects a baercode with invalid signature', function (done) {
            const [version, signedCbor] = baercodeParsing.base64ToVersionAndSignedCbor(b64baercodeMalicious)
            assert.strictEqual(version, 1)
            baercodeParsing.isSignatureValidPemPublicKey(signedCbor, publicKeyPem).then(validSignature => {
                assert.strictEqual(validSignature, false)
                done()
            }).catch(err => console.log('Error in test: ' + err))
        })
    })
    describe('credential data parsing', function () {
        it('has matching data for baercode', function (done) {
            const [version, signedCbor] = baercodeParsing.base64ToVersionAndSignedCbor(b64baercode) // eslint-disable-line no-unused-vars
            baercodeParsing.validateDecodeAndDecryptSignedCborPemKeyAes(signedCbor, publicKeyPem, aesKeyBuffer).then(result => {
                const [valid, cborData] = result
                assert.strictEqual(valid, true)

                const [validBaercode, baercode, errors] = baercodeVerifier.cborToBaerCode(cborData) // eslint-disable-line no-unused-vars
                assert.strictEqual(validBaercode, true)

                assert.strictEqual(baercode.first_name, 'Max')
                assert.strictEqual(baercode.last_name, 'Mustermann')

                // FIXME: Update generator to provide static dates
                assert.strictEqual(
                    baercode.date_of_birth.getTime(),
                    new Date('1990-04-01T00:00:00.000Z').getTime())
                assert.strictEqual(baercode.procedures[0].type, 1)
                assert.strictEqual(
                    baercode.procedures[0].time.getTime(),
                    new Date('2021-05-01T08:00:00.000Z').getTime())
                assert.strictEqual(baercode.procedure_operator, 'PoNC GmbH')

                // The test result is negative, i.e., bool is false
                assert.strictEqual(baercode.procedure_result, false)
                done()
            }).catch(err => console.log('Error in test: ' + err))
        })
    })
})
