# js-BärCODE-verifier

A browser app for verifying BärCODEs.

## Building, running and testing

### Requirements

*  Go version 1.16.4 or later is required for the wasm module.

### Before running or building

* `npm install`
* `npm run build-wasm`

### Run application

* `npm run start`
* Open browser and go to http://localhost:1234

### Building application

* `npm run build`
* Copy contents to root of $WEBSERVER virtual host

### Run tests

Note: The integration tests require generated test data from other repositories, so this is maybe not useful for you.

* `npm run test`

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.
